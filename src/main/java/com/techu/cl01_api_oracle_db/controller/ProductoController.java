package com.techu.cl01_api_oracle_db.controller;

import com.techu.cl01_api_oracle_db.entity.ProductoModel;
import com.techu.cl01_api_oracle_db.service.ProductoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v2")
public class ProductoController {

    @Autowired
    ProductoService productoService;

    @GetMapping("/productos")
    public List<ProductoModel> getProductos(){
        return productoService.findAll();
    }

    @GetMapping("/productos/{id}")
    public Optional<ProductoModel> getProductoId(@PathVariable Integer id){
        return productoService.findById(id);
    }

    @PostMapping("productos")
    public ProductoModel postProductos(@RequestBody ProductoModel productoModel){
        productoService.save(productoModel);
        return productoModel;
    }

    @PutMapping("/productos")
    public void putProductos(@RequestBody ProductoModel productoModel){
        productoService.save(productoModel);
    }

    @DeleteMapping("/productos")
    public boolean deleteProductos(@RequestBody ProductoModel productoModel){
        return productoService.delete(productoModel);
    }
}
