package com.techu.cl01_api_oracle_db.dao;

import com.techu.cl01_api_oracle_db.entity.ProductoModel;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProductoRepository extends JpaRepository<ProductoModel, Integer> {
}
