package com.techu.cl01_api_oracle_db.entity;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCT_TABLE")
public class ProductoModel {

    @Column(name = "ID")
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @Column(name = "DESCRIPTION", nullable = true, length = 255)
    private String descripcion;

    @Column(name = "PRICE", nullable = true)
    private double precio;

    public ProductoModel() {
    }

    public ProductoModel(Integer id, String descripcion, double precio) {
        this.id = id;
        this.descripcion = descripcion;
        this.precio = precio;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
