package com.techu.cl01_api_oracle_db.dao;

import com.techu.cl01_api_oracle_db.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {
}
