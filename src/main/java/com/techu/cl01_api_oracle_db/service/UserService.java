package com.techu.cl01_api_oracle_db.service;

import com.techu.cl01_api_oracle_db.dao.UserDao;
import com.techu.cl01_api_oracle_db.entity.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {

    @Autowired
    UserDao userDao;

    public List<User> getAllUsers(){
        return this.userDao.findAll();
    }

    public User addUser(User user){
        return this.userDao.save(user);
    }
}
